import java.util.*;

public class WordFrequencyGame {

    public static final String CALCULATE_ERROR = "Calculate Error";

    public String generateGameResult(String inputString) {
        List<String> inputWords = generateInputWordList(inputString);
        if (inputWords.size() == 1) {
            return inputString + " 1";
        }
        try {
            List<WordFrequencyNode> wordFrequencyList = generateWordFrequencyList(inputWords);
            return generateWordFrequencyString(wordFrequencyList);
        } catch (Exception e) {
            return CALCULATE_ERROR;
        }
    }

    private static String generateWordFrequencyString(List<WordFrequencyNode> wordFrequencyList) {
        StringJoiner gameResult = new StringJoiner("\n");
        wordFrequencyList.forEach(wordFrequencyNode -> gameResult.add(wordFrequencyNode.getWord() + " " + wordFrequencyNode.getFrequency()));
        return gameResult.toString();
    }

    private List<WordFrequencyNode> generateWordFrequencyList(List<String> wordFrequencyNodeList) {
        Map<String, Integer> map = generateWordFrequencyMap(wordFrequencyNodeList);
        List<WordFrequencyNode> list = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            list.add(new WordFrequencyNode(entry.getKey(),entry.getValue()));
        }
        list.sort((w1, w2) -> w2.getFrequency() - w1.getFrequency());
        return list;
    }

    private List<String> generateInputWordList(String inputString) {
        String[] splitString = inputString.split("\\s+");
        return Arrays.asList(splitString);
    }

    private Map<String, Integer> generateWordFrequencyMap(List<String> wordFrequencyNodeList) {
        Map<String, Integer> map = new HashMap<>();
        wordFrequencyNodeList.forEach(wordFrequencyNode ->map.put(wordFrequencyNode,map.getOrDefault(wordFrequencyNode,0)+1));
        return map;
    }


}
