public class WordFrequencyNode {
    private String word;
    private int frequency;

    public WordFrequencyNode(String w, int i){
        this.word =w;
        this.frequency =i;
    }


    public String getWord() {
        return this.word;
    }

    public int getFrequency() {
        return this.frequency;
    }


}
